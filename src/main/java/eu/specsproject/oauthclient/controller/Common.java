package eu.specsproject.oauthclient.controller;

public class Common {
	public static final String AUTHORIZATION_ENDPOINT = "http://localhost:8080/specs-oauth2-server/oauth/authorize";
	public static final String TOKEN_ENDPOINT = "http://localhost:8080/specs-oauth2-server/oauth/token";
	public static final String PROFILE_ENDPOINT = "http://localhost:8080/specs-oauth2-server/me";

	public static final String REDIRECT_URI = "http://localhost:8080/oauth-test-client/redirect.jsp";

	public static final String CLIENT_ID = "end-user-app";
	public static final String CLIENT_SECRET = "end-user-app-secret";

	public static final String RESPONSE_TYPE_CODE = "code";
	public static final String STATE = "abcdef";
}
