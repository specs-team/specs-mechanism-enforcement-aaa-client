package eu.specsproject.oauthclient.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.oltu.oauth2.client.response.OAuthAuthzResponse;
import org.apache.oltu.oauth2.client.response.OAuthClientResponse;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.apache.oltu.oauth2.common.message.types.ResponseType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class RequestsController {

	@RequestMapping(value="/login",method=RequestMethod.GET)
	public void login(@Context HttpServletRequest request) {
		System.out.println("LOGIN GET CALLED");
		request.setAttribute("authorize_path", Common.AUTHORIZATION_ENDPOINT+"?"+
												"client_id="+Common.CLIENT_ID+"&"+
												"redirect_uri="+Common.REDIRECT_URI+"&"+
												"response_type="+Common.RESPONSE_TYPE_CODE+"&"+
												"state="+Common.STATE);
	}

	@RequestMapping(value="/redirect")
	public ModelAndView redirectReq(ModelMap model, @Context HttpServletRequest request){
		System.out.println("Redirect called from oAuth server");
		try {
			//Get the code form the request
			OAuthClientResponse resp = OAuthAuthzResponse.oauthCodeAuthzResponse(request);
			String code = resp.getParam(ResponseType.CODE.toString());
			System.out.println("Received code: "+code);
			request.setAttribute("code", code);

			//Build the request to get the token using the authorization_code grant type
			OAuthClientRequest requestOauth = OAuthClientRequest
					.tokenLocation(Common.TOKEN_ENDPOINT)
					.setGrantType(GrantType.AUTHORIZATION_CODE)
					.setRedirectURI(Common.REDIRECT_URI)
					.setCode(code)
					.buildBodyMessage();

			//Add Authorization header to the request (not supported in Apache Oltu)
			byte[]   bytesEncoded = org.apache.commons.codec.binary.Base64.encodeBase64((Common.CLIENT_ID+":"+Common.CLIENT_SECRET).getBytes());
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Authorization", "Basic "+new String(bytesEncoded));
			headers.put("Content-Type", "application/x-www-form-urlencoded");

			//Send request
			URLConnectionClient urlConnectionClient = new URLConnectionClient();
			OAuthAccessTokenResponse oAuthResponse = urlConnectionClient.execute(requestOauth, headers,
					"POST", OAuthJSONAccessTokenResponse.class);
			String token = oAuthResponse.getAccessToken();
			System.out.println("Received token: "+token);
			request.setAttribute("token", token);

			//Request the user profile
			HttpURLConnection httpURLConnection = doGetProfileRequest(Common.PROFILE_ENDPOINT, token);
			String profile = parseResponse(httpURLConnection);
			request.setAttribute("profile", profile);
			
			return new ModelAndView("redirect");
		} catch (OAuthProblemException e) {
			e.printStackTrace();
			return new ModelAndView("redirect_error");
		} catch (OAuthSystemException e) {
			e.printStackTrace();
			return new ModelAndView("redirect_error");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return new ModelAndView("redirect_error");
		} catch (IOException e) {
			e.printStackTrace();
			return new ModelAndView("redirect_error");
		}
	}

	@RequestMapping(value="/redirect_error")
	public void redirectErrorReq(ModelMap model, @Context HttpServletRequest request){
		System.out.println("Redirect error called from oAuth server");
	
	}
	public static HttpURLConnection doRequest(OAuthClientRequest req) throws IOException {
		URL url = new URL(req.getLocationUri());
		HttpURLConnection c = (HttpURLConnection)url.openConnection();
		c.setInstanceFollowRedirects(true);
		c.connect();
		c.getResponseCode();

		return c;
	}
	
	public static HttpURLConnection doGetProfileRequest(String urlPath, String token) throws IOException {
		URL url = new URL(urlPath);
		URLConnection c = url.openConnection();

		if (c instanceof HttpURLConnection) {
			HttpURLConnection httpURLConnection = (HttpURLConnection)c;
			httpURLConnection.setRequestMethod("GET");
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setAllowUserInteraction(false);
			httpURLConnection.setRequestProperty("Authorization", "bearer "+token);
			httpURLConnection.connect();

			return httpURLConnection;
		}
		return null;
	}
	
	public static String parseResponse(HttpURLConnection httpURLConnection) throws IOException{
		int responseCode = httpURLConnection.getResponseCode();
		System.out.println("RESPONSE CODE: "+responseCode);
		if(responseCode == 200){
			BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getInputStream())));
			StringBuilder sb = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
			String profile = sb.toString();

			System.out.println("PROFILE RESPONSE: "+profile);
			return profile;
		}else{
			BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getErrorStream())));
			StringBuilder sb = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
			String profile = sb.toString();

			System.out.println("PROFILE RESPONSE: "+profile);
			return profile;
		}	
	}

}
