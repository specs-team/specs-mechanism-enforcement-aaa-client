# README #

CLIENT TEST APPLICATION

The client application uses the apache oltu library and the java.net utils to execute the oauth client step and to support the “Login with SPECS” authentication and authorization.

The project is composed by two main classes:

RequestsController:
It contains three endpoints: login, redirect, redirect_error.

 - The login end-point is useful to set the parameters to call the authorize api on the oauth server when a “Login with SPECS” button is clicked (the button is into the login.jsp page under WEB-INF/jsp)

 - The redirect and point is called from the oauth server after the user authentication and authorization phase.
So, when the redirect end point is called, a request to the oauth server (token end point) is built and, subsequently a request to the oauth server (me end point) is built to receive the end-user profile.

 - The redirect_error endpoint is called when an error during the authorization face occurs. In particular, for example, when the user deny the authorization to the client application to read his parameters.

Common:
This class contains all the static parameters used into the test application: client id, client secret and so on.